# Segun Bitcoin Docker

To start a node, simply install docker and run

<code>
    docker-compose up
</code>

To  run  in the  background (as a daemon)

<code>
    docker-compose up -d
</code>